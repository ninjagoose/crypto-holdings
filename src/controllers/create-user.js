// Create user module

var _ = require('lodash-node'),
	Q = require('q'),
	bcrypt = require('bcrypt'),
	shortId = require('shortid');

module.exports = function(Users,FireRef,Postmark,EmailTemplates,tokenGenerator) {

	return [
		function(req,res,next) {
			console.log('I should validate here....', req.body);
			next();
		},
		function(req,res,next) {
			var data = req.body,
				_user = _.omit(data,['passphrase']),

				error = function(err) {
					console.log(err);
					next(err);
				},

				encrypting = Q.defer(),
				saving = Q.defer(),
				verifying = Q.defer();

			console.log('this is a test again???', data);

			// User bcrypt to hash password
			bcrypt.hash(data.passphrase, 12, encrypting.makeNodeResolver());

			// Handling encrypting password
			encrypting.promise.then(function(hash) {
				// var Users = $db.collection('users');

				try {
					_user.pw = hash;
					_user.url = shortId.generate();
					_user.saved_at = Date.now();

					console.log('inserting session document', _user);
					Users.insert(_user, saving.makeNodeResolver());
				} catch(e) {
					console.log('what is the hell',e);
				}
			}, function(err) {
				console.log(err);
				next(err);
			});

			// Handling saving the user data
			saving.promise.then(function(docs) {
				var user = docs[0],
					userRef = FireRef.child('users/'+user.url),
					fireSetting = Q.defer(),

					token = tokenGenerator.createToken({
						_id: user._id,
						url: user.url,
						name: user.name
					});

				req.session.user = user;
				req.session.user_token = token;

				userRef.set(user, fireSetting.makeNodeResolver());

				// Firebase setting of user data
				fireSetting.promise.then(function() {
					console.log('user document should be saved and new token generated', token);
					res.json({ token: token });
				}, error);
			}, error);
		}
	];
};
