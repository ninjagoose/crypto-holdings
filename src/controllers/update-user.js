// User update module

var _ = require('lodash-node'),
	Q = require('q');

module.exports = function(Users,FireRef,MongoObjectId) {

	return [
		function(req,res,next) {
			var user = req.session.user,
				updating = [ 'name', 'email' ],
				data = _.pick(req.body, updating),
				mongoUpdating = Q.defer();
				// fireSetting = Q.defer();

			// Q.all([ mongoUpdating.promise, fireSetting.promise ]).spread(function() {
			mongoUpdating.promise.then(function() {

				req.session.user = _.extend(user, data);

				res.json({
					success: 'Profile was updated successfully'
				});
			}, function(err) {
				console.log(err);
				res.json({
					err: err
				});
			});

			console.log('setting the user data...', user._id, data);
			Users.update({ '_id': new MongoObjectId(user._id) }, { '$set': data }, { 'safe':true }, mongoUpdating.makeNodeResolver());
			// FireRef.child('users/'+user._id).set(user, fireSetting.makeNodeResolver());
		}
	];
};
