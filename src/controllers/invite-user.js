// User invite module

var _ = require('lodash-node'),
	Q = require('q');

module.exports = function(Users, FireRef, MongoObjectId, EmailTemplates, Postmark) {

	return [
		function(req,res,next) {
			var user = req.session.user;

			if(!user) {
				return res.json({
					err: 'No user session'
				});
			}

			var inviteRef = FireRef.child('users/'+user._id+'/invites').push(),
				invitation = {
					id: inviteRef.name(),
					email: req.body.email
				},

				fire = Q.defer(),
				updating = Q.defer(),
				inviting = Q.defer();

			console.log('inviting noob from ', user, 'to', invitation);

			inviteRef.set(invitation, fire.makeNodeResolver());
			Users.update({ _id: new MongoObjectId(user._id) }, { $push: { invites: invitation } }, updating.makeNodeResolver());

			EmailTemplates('invite-user', { user: user, url: 'http://localhost:5000/auth.html#/join/' + invitation.id }, function(err, html, text) {
				if(err) {
					return inviting.reject(err);
				}

				Postmark.send({
					"From": "s@ninjastack.com",
					"To": invitation.email,
					"ReplyTo": user.email,
					"Subject": "You've been invited to Join!",
					"TextBody": text,
					"HtmlBody": html
				}, inviting.makeNodeResolver());
			});

			Q.all([ inviting.promise, updating.promise, fire.promise ]).then(function(data) {
				console.log('inviting was successful', data);
				res.json({
					success: 'Invite sent to ' + invitation.email
				});
			}, function(err) {
				console.log('error while inviting', err);
				res.json({
					err: 'Error while sending invite'
				});
			});
		}
	];
};
