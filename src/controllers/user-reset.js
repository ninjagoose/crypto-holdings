// User reset module

var _ = require('lodash-node'),
	Q = require('q'),
	bcrypt = require('bcrypt');

module.exports = function(Users) {

	return [
		function(req,res,next) {
			var data = req.body,

				encrypting = Q.defer(),
				resetting = Q.defer(),

				valid = function(user,hash) {
					Users.findAndModify({ _id: user._id }, [], { '$set': { 'pw': hash }, '$unset': { reset: "" } }, { }, resetting.makeNodeResolver());
				},
				invalid = function(err) {
					res.json({ err: err });
				};

			// User bcrypt to hash password
			bcrypt.hash(data.password, 12, encrypting.makeNodeResolver());

			// Handling encrypting password
			encrypting.promise.then(function(hash) {
				if(data.uid) {
					Users.findAndModify({ reset: data.uid }, [], { '$set': { 'pw': hash }, '$unset': { reset: "" } }, {  }, resetting.makeNodeResolver());
				} else if(req.session.user) {
					bcrypt.compare(data.current, req.session.user.pw, function(err,check) {
						if(err) {
							console.log(err);
							return next(err);
						}

						if(check) {
							valid(req.session.user,hash);
						} else {
							invalid('Password is incorrect');
						}
					});
				} else {
					invalid('User is not logged in properly');
				}
			}, function(err) {
				console.log(err);
				next(err);
			});

			// Handling resetting the user password
			resetting.promise.then(function(data) {
				var user = data[0];

				res.json({ success: 'Password was reset successfully' });
			}, function(err) {
				invalid(err);
			});
		}
	];
};
