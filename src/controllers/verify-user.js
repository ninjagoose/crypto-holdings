// Verify user module

var _ = require('lodash-node'),
	Q = require('q');

module.exports = function(Users,FireRef) {

	return [
		function(req,res,next) {
			var data = req.body,
				id = data.uid,

				verifying = Q.defer();

			verifying.promise.then(function(data) {
				var user = data[0],
					meta = data[1];

				console.log('setting some firebase stuff', user._id, user.verified_at);
				FireRef.child('users/'+user._id+'/verified').set(user.verified_at, function(err) {
					console.log('verification has been set in firebase');
					if(err) {
						console.log(err);
					}
				});

				res.json({
					success: user.name + ' was verified successfully'
				});
			}, function(err) {
				console.log(err);
				res.json({
					err: 'Error while verifying'
				});
			});

			Users.findAndModify({ 'verify': id }, [], { '$set': { 'verified': true, 'verified_at': Date.now() } }, { 'new':true }, verifying.makeNodeResolver());
		}
	];
};
