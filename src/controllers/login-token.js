// Login token module

module.exports = function() {
	return [
		function(req,res,next) {
			var token = req.session.user_token;

			if(token) {
				res.json({
					token: req.session.user_token
				});
			} else {
				res.json({
					err: 'No user session'
				});
			}
		}
	];
};