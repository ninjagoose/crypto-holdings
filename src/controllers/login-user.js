// Create user module

var _ = require('lodash-node'),
	Q = require('q'),
	bcrypt = require('bcrypt');

module.exports = function(Users,tokenGenerator) {

	return [
		function(req,res,next) {
			console.log('login validation here', req.body);
			next();
		}, function(req,res,next) {
			var data = req.body,

				finding = Q.defer(),

				invalid = function() {
					console.log('invalid user!!');

					res.json({
						err: 'Invalid Email or Password'
					});
				},
				valid = function(user) {
					var token = tokenGenerator.createToken({
						id: user._id,
						name: user.name
					});

					req.session.user = user;
					req.session.user_token = token;

					res.json({
						token: token
					});
				};

			finding.promise.then(function(user) {
				console.log('have user?',user);
				if(user) {
					console.log('start compare');
					bcrypt.compare(data.password, user.pw, function(err,check) {
						console.log('compared',arguments);
						if(err) {
							console.log(err);
							return next(err);
						}

						if(check) {
							valid(user);
						} else {
							invalid();
						}
					});
				} else {
					invalid();
				}
			}, function(err) {
				console.log(err);
				next(err);
			});

			// Lookup user doc from mongo by email, we only want the pw hash
			Users.find({ email: data.email },{ pw:1, name:1 }).nextObject(finding.makeNodeResolver());
		}
	];
};
