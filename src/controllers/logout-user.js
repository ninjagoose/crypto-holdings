// Logout module

module.exports = function() {
	return [
		function(req,res,next) {
			delete req.session.user;
			delete req.session.user_token;
			res.json({ success: 'User has been logged out' });
		}
	];
};