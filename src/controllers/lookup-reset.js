// Forgot password module

var _ = require('lodash-node'),
	Q = require('q');

module.exports = function(Users) {

	return [
		function(req,res,next) {
			var data = req.query,
				finding = Q.defer();

			console.log('user lookup by reset',data);

			finding.promise.then(function(obj) {
				res.json(_.pick(obj,['name','email']));
			}, function(err) {
				console.log('user lookup error', err);
				res.json({ err: err });
			});

			Users.find({ reset: data.query }).nextObject(finding.makeNodeResolver());
		}
	];
};
