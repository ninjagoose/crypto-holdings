// save feedback module

var _ = require('lodash-node'),
	Q = require('q');

module.exports = function(Feedback) {

	return [
		function(req,res,next) {
			var data = req.body;

			Feedback.insert(data, function(err,docs) {
				if(err) {
					res.status(500).send('Error saving feedback, try again in a minute');
				}
				res.send('Saved feedback, thanks!!');
			});
		}
	];
};
