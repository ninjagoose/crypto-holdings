// Forgot password module

var _ = require('lodash-node'),
	Q = require('q'),
	shortId = require('shortid');

module.exports = function(Users,Postmark,EmailTemplates) {

	return [
		function(req,res,next) {
			var email = req.body.email,

				resetting = Q.defer(),
				error = function(err) {
					console.log('error stuff,',err);
					res.json({
						'err': err
					});
				};

			Users.findAndModify({ 'email': email }, [], { '$set': { 'reset': shortId.generate() } }, { 'new': true }, resetting.makeNodeResolver());

			resetting.promise.then(function(data) {
				var user = data[0],
					obj = data[1];

				if(user) {
					EmailTemplates('forgot-pw', { url: 'http://localhost:5000/auth.html#/password/reset/'+user.reset }, function(err, html, text) {
						if (err) {
							return error(err);
						}

						// Send a forgot password email
						Postmark.send({
							"From": "s@ninjastack.com",
							"To": user.email,
							"Subject": "Looks like you forgot a password",
							"TextBody": text,
							"HtmlBody": html
						}, function(err,data) {
							if(err) {
								return error(err);
							}

							console.log('POSTMARK DATA: ',data);
							res.json(data);
						});
					});
				} else {
					error('No user found');
				}
			}, function(err) {
				error(err.message);
			});
		}
	];
};
