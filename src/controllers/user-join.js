// User join module

var _ = require('lodash-node'),
	Q = require('q'),
	bcrypt = require('bcrypt');

module.exports = function(Users, FireRef, MongoObjectId) {

	return [
		function(req,res,next) {
			var user = req.session.user;

			if(!user) {
				return res.json({
					err: 'No user session'
				});
			}

			var uid = req.body.uid,

				mongoFind = Q.defer(),
				fireSet = Q.defer(),
				mongoUpdate = Q.defer();

			console.log('what is the shit happening?', uid);
			// Users.findAndModify({ invites: { id: uid } }, [], { }, { new:true }, mongoFind.makeNodeResolver());
			Users.find({ 'invites.id': uid }).nextObject(mongoFind.makeNodeResolver());

			mongoFind.promise.then(function(invited_by) {
				// var invited_by = obj[0],
				// meta = obj[1];

				if(invited_by) {
					var invitation = _.pick(invited_by, [ 'name', 'email' ]);
					invitation.user = invited_by._id;
					invitation.invite = uid;

					console.log('we have found your invitation', invitation);

					Users.update({ _id: new MongoObjectId(user._id) }, { $set: { invited_by: invitation } }, mongoUpdate.makeNodeResolver());
					FireRef.child('users/'+user._id+'/invited_by').set(invitation,fireSet.makeNodeResolver());
				} else {
					res.json({
						err: 'Invalid invitation'
					});
				}
			}, function(err) {
				console.log('something happening',err);
				res.json({
					err: 'Error looking up invitation'
				});
			});

			Q.all([ fireSet.promise, mongoUpdate.promise ]).then(function(data) {
				res.json({
					success: 'Welcome, you are now a member!'
				});
			}, function(err) {
				console.log('something happened',err);
				res.json({
					err: err
				});
			});
		}
	];
};
