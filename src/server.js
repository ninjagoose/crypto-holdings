// Boilerplate Express Web Server

console.log('Begin web server start up...');

// Load main web application server modules
var express = require('express'),
	app = express(),
	port = process.env.PORT || 5000,
	
	// Load utility modules
	http = require('http'),
	path = require('path'),
	_ = require('lodash-node'),
	Q = require('q'),

	bcrypt = require('bcrypt'),
	shortId = require('shortid'),
	EmailTemplateLoader = require('email-templates'),

	// Application Keys and Secrets, should be kept elsewhere
	SESSION_SECRET = '$2a$12$bF3KEdJTxITj/us1DkH20.u.V9yVWry8RC1b9qNQeRoPXLDZitxfu',
	POSTMARK_API_KEY = process.env['POSTMARK_API_KEY'] || 'c9659a82-9e44-4ac1-b0ca-2542f2e420d4',
	FIREBASE_SECRET = process.env['FIREBASE_SECRET'] || 'fSlkuHeCk1UmGcUNQw1y6SRchHu0XXXdezTvYRMH',
	
	// Connection URLs
	MONGO_URL = process.env['MONGOHQ_URL'] ||  'mongodb://localhost/cryptoholdings',
	FIREBASE_URL =  process.env['FIREBASE_URL'] || 'https://cryptoholdings.firebaseio.com/',

	// MongoDB storage
	MongoClient = require('mongodb').MongoClient,
	MongoObjectId = require('mongodb').ObjectID,
	MongoStore = require('connect-mongo')(express),

	// Firebase storages
	Firebase = require('firebase'),
	FirebaseTokenGenerator = require("firebase-token-generator"),
	fireRef = new Firebase(FIREBASE_URL),
	tokenGenerator = new FirebaseTokenGenerator(FIREBASE_SECRET),

	// Cryptsy
	Cryptsy = require('cryptsy'),
	cryptsy = new Cryptsy('e04ca65c109e1a17fcc601b02287338948e50d83', 'f6b446e82eebd90033911ed307f1ba14a830cbfd94e387e4c3c1e89dc852469cd6106f493f9a33b5'),

	// Coinbase
	Coinbase = require('coinbase'),
	coinbase = new Coinbase({
		APIKey: 'f1d5c79049a3b0d8f42ed8ab9a82793221b2e448e23eefd6613fe0ad8d586529'
	}),

	// Paths
	controllerPath = path.resolve(__dirname, 'controllers'),
	emailTemplatePath = path.resolve(__dirname, 'templates', 'emails'),
	
	// Email Service
	Postmark = require("postmark")(POSTMARK_API_KEY),

	// Initialization promises
	dfr_EmailTemplateLoading = Q.defer(),
	dfr_FirebaseAuth = Q.defer(),
	dfr_DatabaseConnecting = Q.defer(),
	dfr_ServerListening = Q.defer(),

	// Error loggin
	logError = function(err) {
		console.log(err);
	};

// Express Configuration and Middleware Stack
app.configure(function(){
	// Sessions and Cookies
	app.use(express.cookieParser());
	app.use(express.session({
		store: new MongoStore({
			url: MONGO_URL,
			auto_reconnect: true
		}),
		secret: SESSION_SECRET
	}));

	// Static serving
	app.use(express.static(__dirname + '/public'));
	app.use(express.favicon(__dirname + '/public/assets/favicon.ico'));

	// Parse request body
	app.use(express.bodyParser());

	// App server router
	app.use(app.router);

	// 404 serving
	app.use(function(req,res,next) {
		res.status(404).send('404 - Not Found');
	});

	// Error serving
	app.use(function(err, req, res, next){
		console.error(err.stack);
		res.status(500).send('500 - Unknown Server Error, terribly sorry');
	});
});

// After server begins listening
dfr_ServerListening.promise.then(function() {
	console.log('Listening on port ' + port);
}, logError);

// After email templates are loaded and mongo database connection established
Q.all([ dfr_DatabaseConnecting.promise, dfr_EmailTemplateLoading.promise, dfr_FirebaseAuth.promise ]).spread(function(DB, EmailTemplates, AuthError) {
	console.log('Connections have been made, setup server routes and background functions');

	// DB collection models
	var Users = DB.collection('users'),
		Feedback = DB.collection('feedback'),

		cryptsyMktsRef = fireRef.child('xchg/cryptsy/mkts'),
		cryptsyOrdersRef = fireRef.child('xchg/cryptsy/orders'),

		cryptsyUpdateDelay = 10000,
		cryptsyErrors = 0,
		cryptsyTimeout,
		cryptsyUpdate = function cryptsyUpdate() {
			var marketDfr = Q.defer(),
				dataDfr = Q.defer();

			fireRef.child('xchg/cryptsy/last_attempt').set((new Date()).toString());

			cryptsy.api('marketdatav2', null, dataDfr.makeNodeResolver());
			cryptsy.api('getmarkets', null, marketDfr.makeNodeResolver());

			Q.all([ marketDfr.promise, dataDfr.promise ]).spread(function(markets, data) {

				markets = _.map(markets, function(mkt) {
					return _.extend(mkt, _.pick(_.find(_.values(data.markets), { marketid: mkt.marketid }), [ 'lasttradetime', 'buyorders', 'sellorders' ]));
				});

				markets = _.object(_.pluck(markets,'marketid'),markets);


				cryptsyMktsRef.set(markets, function(err) {
					if(err) {
						console.log('error setting marketdata', err);
						return;
					}

					console.log('marketdata set confirmed....');
				});

				if(cryptsyErrors) {
					cryptsyErrors = 0;
					fireRef.child('xchg/cryptsy/error').set(null);
					fireRef.child('xchg/cryptsy/error_count').set(null);
				}

				fireRef.child('xchg/cryptsy/last_success').set((new Date()).toUTCString());
				cryptsyErrors=0;
				cryptsyTimeout = setTimeout(cryptsyUpdate,cryptsyUpdateDelay);
			}, function(err) {
				console.log('error getting cryptsy data', err);
				cryptsyErrors++;
				cryptsyTimeout = setTimeout(cryptsyUpdate,cryptsyUpdateDelay*cryptsyErrors);

				fireRef.child('xchg/cryptsy/error').set('error loading cryptsy api data');
				fireRef.child('xchg/cryptsy/error_count').set(cryptsyErrors);
				fireRef.child('xchg/cryptsy/last_error').set((new Date()).toUTCString());
			});
		},

		coinbaseRef = fireRef.child('xchg/coinbase/mkts'),
		coinbaseUpdateDelay = 10000,
		coinbaseErrors = 0,
		coinbaseUpdate = function coinbaseUpdate() {
			fireRef.child('xchg/coinbase/last_attempt').set((new Date()).toUTCString());
			coinbase.currencies.exchangeRates(function (err, data) {
				if(err) {
					console.log('error getting coinbase data', err);
					
					coinbaseErrors++;
					fireRef.child('xchg/coinbase/error').set('error loading coinbase api data');
					fireRef.child('xchg/coinbase/error_count').set(coinbaseErrors);
					fireRef.child('xchg/coinbase/last_error').set((new Date()).toUTCString());
					setTimeout(coinbaseUpdate,coinbaseUpdateDelay*coinbaseErrors);
					return;
				}

				coinbaseRef.child('btc_to_usd').set(data.btc_to_usd, function(err) {
					if(err) {
						console.log('coinbase data setting error', err);
						return;
					}	

					console.log('coinbase data set confirmed');
				});

				if(coinbaseErrors) {
					coinbaseErrors = 0;
					fireRef.child('xchg/coinbase/error').set(null);
					fireRef.child('xchg/coinbase/error_count').set(coinbaseErrors);
				}
				
				fireRef.child('xchg/coinbase/last_success').set((new Date()).toUTCString());
				setTimeout(coinbaseUpdate,coinbaseUpdateDelay);
			});
		};

	cryptsyUpdate();
	coinbaseUpdate();

	// Routing Controllers
	// - Create new user
	app.post('/user/create/?', require(controllerPath + '/create-user')(Users,fireRef,Postmark,EmailTemplates,tokenGenerator));
	// - Verify user
	// app.post('/user/verify', require(controllerPath + '/verify-user')(Users,fireRef));
	// - Get User login token
	app.get('/user/login/?', require(controllerPath + '/login-token')());
	app.post('/feedback/?', require(controllerPath + '/save-feedback')(Feedback));
	// // - Log user in
	// app.post('/user/login/?', require(controllerPath + '/login-user')(Users,tokenGenerator));
	// // - Log user out
	// app.post('/user/logout', require(controllerPath + '/logout-user')());
	// // - User forgot password
	// app.post('/user/forgot-password/?', require(controllerPath + '/forgot-password')(Users,Postmark,EmailTemplates));
	// // - Lookup user by reset id
	// app.get('/user/lookup/reset/?', require(controllerPath + '/lookup-reset')(Users,fireRef,MongoObjectId));
	// // - User Join from invitation
	// app.post('/user/join/?', require(controllerPath + '/user-join')(Users,fireRef,MongoObjectId));
	// // - Invite new users
	// app.post('/invite/?', require(controllerPath + '/invite-user')(Users,fireRef,MongoObjectId,EmailTemplates,Postmark));
	// // - User profile
	// app.post('/user/update', require(controllerPath + '/update-user')(Users,fireRef,MongoObjectId));

	// - Catch all the errors that aren't explicitly thrown
	app.all('*', require(controllerPath + '/check-error')());

	// Create server and start listening
	http.createServer(app).listen(port, dfr_ServerListening.makeNodeResolver());
}, logError);

// Load email templates
EmailTemplateLoader(emailTemplatePath, dfr_EmailTemplateLoading.makeNodeResolver());
// Connect to mongo database
MongoClient.connect(MONGO_URL, dfr_DatabaseConnecting.makeNodeResolver());
// Authenticate firebase
fireRef.auth(FIREBASE_SECRET, dfr_FirebaseAuth.makeNodeResolver());

// End of server code
