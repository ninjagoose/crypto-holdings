angular.module( 'njaFilters', [])

.filter('isLoggedIn', [ function() {
	return function(user) {
		return user && !user.loading && !!user.authentication;
	};
}])

.filter('isLoggedOut', [ function() {
	return function(user) {
		return !user || (!user.loading && !user.authentication);
	};
}])

.filter('isLoading', [ function() {
	return function(user) {
		return user && !!user.loading;
	};
}])

.filter('isEmpty',[ 
	function() {
		return function(input) {
			return _.isEmpty(input);
		};
	}
])

.filter('toDate',[ 
	function() {
		return function(input) {
			return input ? new Date(input) : input;
		};
	}
])

.filter('toggleExponential', function() {
	return function(input,d,f) {
		if(typeof d === 'boolean') {
			if(d) {
				return parseFloat(input,10).toExponential(f);
			} else {
				return parseFloat(input,10).toFixed(d);
			}
		} else {
			if(typeof f === 'boolean') {
				if(f) {
					return parseFloat(input,10).toExponential(d);
				} else {
					return parseFloat(input,10).toFixed(d);
				}
			} else {
				return parseFloat(input,10).toFixed(d);
			}
		}
	};
})
.filter('toPrecision', function() {
	return function(input,p) {
		return p ? parseFloat(input).toPrecision(p) : parseFloat(input);
	};
})

.filter('toFixed', function() {
	return function(input,p) {
		return p ? parseFloat(input).toFixed(p) : parseFloat(input);
	};
})

.filter('sum', function() {
	return function(input) {
		return _.reduce(input,function(n,memo) {  return n+memo; }, 0);
	};
})
.filter('capitalize',[ 
	function() {
		return function(input) {
			return input.charAt(0).toUpperCase() + input.slice(1);
		};
	}
])
.filter('ifelse',[ 
	function() {
		return function(input,_if,_else) {
			return input ? _if : _else;
		};
	}
])


;
