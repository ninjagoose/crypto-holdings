angular.module( 'njaDirectives', [])

.directive('preventDefault', function() {
	return function(scope,el,attrs) {
		var evt = attrs.preventDefault || 'click';
		
		el.bind(evt, function(e) {
			e.preventDefault();
		});
	};
})

.directive('stopPropagation', function() {
	return function(scope,el,attrs) {
		var evt = attrs.stopPropagation || 'click';

		el.bind(evt, function(e) {
			e.stopPropagation();
		});
	};
})
.directive('enterKeyup', function() {
	return function(scope,el,attrs) {
		el.bind("keyup", function(e) {
			if(e.which===13) {
				scope.$apply(attrs.enterKeyup);
			}
		});
	};
})

.directive('selectBlur', function() {
	var evt = $.Event("keydown", { keyCode: 13, which: 13 });
	return {
		link: function(scope,el,attrs) {
			el.bind("blur", function(e) {
				el.trigger(evt);
			});
		}
	};
})

.directive('comingSoon', function() {
	return function(scope,el,attrs) {
		var evt = attrs.preventDefault || 'click';
		
		el.bind(evt, function(e) {
			alert('coming soon');
		});
	};
})

;
