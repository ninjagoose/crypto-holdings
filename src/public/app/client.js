angular.module( 'cryptoFreedom', [ 'ngCookies', 'ui.bootstrap', 'angular-lodash', 'firebase', 'njaFilters', 'njaServices', 'njaDirectives' ])

.config([ '$routeProvider', '$fireRefProvider',
	function ($routeProvider, $fireRefProvider) {

		$routeProvider
			.when('/', { 
				templateUrl: '/app/views/home.html',
				controller: 'DefaultCtrl'
			})
			.when('/start', {
				templateUrl: '/app/views/start.html',
				controller: 'StartCtrl'
			})
			.when('/s/:url', {
				templateUrl: '/app/views/stash.html',
				controller: 'StashCtrl'
			})
			.when('/contributing', {
				templateUrl: '/app/views/contributing.html',
				controller: 'ContributeCtrl'
			})
			.when('/learn', {
				templateUrl: '/app/views/learn.html',
				controller: 'DefaultCtrl'
			})
			.when('/thoughts', {
				templateUrl: '/app/views/thoughts.html',
				controller: 'DefaultCtrl'
			})
			.when('/about', {
				templateUrl: '/app/views/about.html',
				controller: 'DefaultCtrl'
			})
			.otherwise({ redirectTo: '/' });

		$fireRefProvider.options.url = 'https://cryptoholdings.firebaseio.com/';
	}
])

.service('AppState',
	function() {
		return {
			game: {
				toExp: false,
				expDec: 8,
				goal_coin: 'USD',
				active_exchange: 'cryptsy',
				player: {
					stash: {
					}
				},
				xchg: {}
			}
		};
	}
)
.filter('total_btc_stash', function() {
	return function(stash,p) {
		if(!stash || _.isEmpty(stash)) {
			return 0;
		}

		var tots = _.reduce(_.values(stash),function(memo,coin) {
			return memo+(coin.qty*coin.last_trade);
		},0);
		return p ? parseFloat(tots).toPrecision(p) : tots;
	};
})

.service('XchgSvc', [
	'AppState','$rootScope','$fireRef',
	function(AppState, $rootScope, $fireRef) {
		var xRef = $fireRef.child('xchg'),
			_svc = {
				listening: {},
				listen: function(to,cb,isTru) {

					if(!_svc.listening[to]) {
						_svc.listening[to] = function(snap) {
							cb(snap.val());
						};

						xRef.child(to).on('value', _svc.listening[to], isTru);
					}
				},
				unlisten: function(to) {
					if(_svc.listening[to]) {
						xRef.child(to).off('value', _svc.listening[to]);
						delete _svc.listening[to];
					}
				},
				spy: function(on,cb) {
					_svc.listen(on,function() {
						cb.apply(this,arguments);
						
						if(!$rootScope.$$phase) {
							$rootScope.$digest();
						}
					});
				},
				truSpy: function(on,record) {
					_svc.listen(on,function(val) {
						AppState.game.xchg[record] = val;
						if(!$rootScope.$$phase) {
							$rootScope.$digest();
						}
					},true);
				}
			};

		return _svc;
	}
])

.service('GameSvc', [
	'AppState', '$rootScope', '$fireRef', 'XchgSvc', '$user',
	function(AppState, $rootScope, $fireRef, XchgSvc, $user) {
		var _svc = {
			extend: function(game) {
				angular.extend(AppState.game,game);
				if(!$rootScope.$$phase) {
					$rootScope.$digest();
				}
			},
			xlisten: function(exchange) {
				exchange = exchange || 'cryptsy';

				// REMOVED XCHGSVC CALL

				$fireRef.child('xchg/'+exchange+'/mkts').on('value',function(snap) {
					var val = snap.val(),
						stash = AppState.game.player.stash;

					if(val) {
						if(!AppState.game.xchg[exchange]) {
							AppState.game.xchg[exchange] = {};
						}
						AppState.game.xchg[exchange].mkts = val;

						_(AppState.game.xchg[exchange].mkts).filter(function(mkt) {
							return mkt.secondary_currency_code==='BTC' && _.contains(_.keys(stash), mkt.primary_currency_code);
						}).each(function(mkt) {
							angular.extend(stash[mkt.primary_currency_code], _.omit(mkt,['buyorders','sellorders','recenttrades']));
						});

						if(!$rootScope.$$phase) {
							$rootScope.$digest();
						}
					}
				});
			},
			unxlisten: function(exchange) {
				exchange = exchange || 'cryptsy';
				_.each(AppState.game.xchg[exchange], function(val,key) {
					XchgSvc.unlisten(key);
				});
			},
			pause: function() {
				_svc.unxlisten();
				AppState.game.on = false;
			},
			unpause: function() {
				_svc.xlisten();
				AppState.game.on = true;
			},
			loadOrders: function(mkt,exchange) {
				var coin = AppState.game.player.stash[mkt.primary_currency_code];

				exchange = exchange || 'cryptsy';

				XchgSvc.spy(exchange+'/mkts/'+mkt.marketid+'/sellorders',function(val) {
					coin.sellorders = val;
					coin.sellsort = 'sellprice';
				});

				XchgSvc.spy(exchange+'/mkts/'+mkt.marketid+'/buyorders',function(val) {
					coin.buyorders = val;
					coin.buysort = 'buyprice';
				});
			},
			setGoal: function(goal,player) {
				angular.extend(AppState.game,goal);
				angular.extend(AppState.game.player,player);
			},
			start: function() {
				// var gRef = $fireRef.child('games').push();
				
				// AppState.game._id = gRef.name();
				AppState.game.started_at = Date.now();
				AppState.game.remaining_goal = AppState.game.goal;
				
				AppState.game.on = true;
				
				$rootScope.success = 'Commence CryptoFreedom!';
			},
			save: function(session) {
				// change to $session service
				return $user.create(session);
			},
			load: function(auth) {
				// LOGIN AND AUTHENTICATE WITH TOKEN HERE

				// change to $session service
				$user.login(auth).then(function(session) {
					$fireRef.child('games').child(session._id).once('value', function(snap) {
						var sess = snap.val();
						angular.extend(AppState.game,sess);
						
						$rootScope.success = 'Game Load!';
						if(!$rootScope.$$phase) {
							$rootScope.$digest();
						}
					});
				});
			},
			calculateState: function() {
				AppState.game.player.stash_btc = _.reduce(_.values(AppState.game.player.stash),function(memo,coin) {
					return memo+(coin.qty*coin.last_trade);
				},0);
			},
			addToStash: function(order, stash, exchange) {
				exchange = exchange || 'cryptsy';
				stash = stash || AppState.game.player.stash;

				if(!order) {
					$rootScope.err = 'Seems you entered nothing...';
					return;
				}

				if(order.csv) {
					console.log('what ths shit', AppState.game);
					_.each(order.csv.split('\n'), function(cStr) {
						var _c = cStr.split(/,\s*/),
							coin = angular.copy(_.omit(_.find(_.values(AppState.game.xchg[exchange].mkts),{ primary_currency_code: _c[0] }), ['buyorders','sellorders','recentrades']));

						if(!coin || _.isEmpty(coin)) {
							return;
						}

						coin.qty = parseFloat(_c[1],10);

						// Check for coin in existing stash
						if(!stash[coin.primary_currency_code]) {
							// assign if not present
							stash[coin.primary_currency_code] = coin;
						} else {
							// addo to quantity if we've already got the coin
							stash[coin.primary_currency_code].qty += coin.qty;
						}
					});

					return;
				}

				if(!order.qty) {
					$rootScope.err = 'You did not enter the quantity of coin.';
					return;
				}

				if(!order.coin || !_.isObject(order.coin)) {
					$rootScope.err = 'You did not specify a valid coin.';
					return;
				}

				var coin = angular.copy(order.coin);

				coin.qty = parseFloat(order.qty,10);
				delete order.qty;

				// Check for coin in existing stash
				if(!stash[coin.primary_currency_code]) {
					// assign if not present
					stash[coin.primary_currency_code] = coin;
				} else {
					// addo to quantity if we've already got the coin it
					stash[coin.primary_currency_code].qty += parseFloat(coin.qty,10);
				}

				_svc.calculateState();
			},
			exchange: function(trade,exchange) {
				var stash = AppState.game.player.stash,
					from_code = trade.from_coin.primary_currency_code,
					to_code = trade.to_coin.primary_currency_code,
					from_coin = stash[from_code],
					to_coin = stash[to_code];

				exchange = exchange || 'cryptsy';

				if(from_coin.qty<trade.from_qty) {
					$rootScope.err = 'Not enough of ' + from_coin.primary_currency_name + ' in stash.';
					return;
				}

				from_coin.qty -= trade.from_qty;
				if(from_coin.qty<=0) {
					delete stash[from_code];
				}

				if(!to_coin) {
					to_coin = stash[to_code] = angular.copy(_.find(_.values(AppState.game.xchg[exchange].mkts),{ primary_currency_code: to_code }));
				}

				to_coin.qty = (to_coin.qty || 0) + trade.to_qty;

				if(!$rootScope.$$phase) {
					$rootScope.$digest();
				}
			},
			cashOut: function(trade) {
				var stash = AppState.game.player.stash,
					btc_price = AppState.game.btc_price,
					cashout_btc = 0,
					msg;

				trade.stash = _.omit(trade.stash, function(coin) {
					return !coin.qty;
				});

				_.each(trade.stash, function(coin,id) {
					var qty = parseFloat(coin.qty,10);

					if(!qty || !id) {
						return;
					}

					if(!stash[id]) {
						$rootScope.err = 'You can\'t cash out of ' + stash[id].primary_currency_name + ' because you do not have any in your stash.';
						return;
					}

					if(stash[id].qty<qty) {
						$rootScope.err = 'You don\'t have ' + qty + ' ' + stash[id].primary_currency_name + ' to cash out of your stash.';
						return;
					}
					
					stash[id].qty -= qty;
					cashout_btc += qty*parseFloat(coin.last_trade,10);

					if(stash[id].qty<=0) {
						delete stash[id];
					}
				});

				if(trade.to_goal) {
					if(AppState.game.goal_coin==='USD') {
						AppState.game.remaining_goal -= cashout_btc*btc_price;
						msg = 'You cashed out ' + _.keys(trade.stash).join(', ') + ' for ' + (cashout_btc*btc_price).toFixed(2) + 'USD toward your goal of ' + AppState.game.goal + 'USD.';
					} else {
						AppState.game.remaining_goal -= cashout_btc;
						msg = 'You cashed out ' + _.keys(trade.stash).join(', ') + ' for ' + cashout_btc + 'BTC toward your goal of ' + AppState.game.goal + 'BTC.';
					}
				} else {
					msg = 'You removed ' + _.keys(trade.stash).join(', ') + ' from your stash.';
				}

				$rootScope.success = msg;

				if(!$rootScope.$$phase) {
					$rootScope.$digest();
				}
			},
			coinIn: function(trade,exchange) {
				var stash = AppState.game.player.stash,
					cashin = _.reduce(trade.stash, function(memo,coin) {
						return memo + (coin.last_trade*coin.qty);
					},0);

				exchange = exchange || 'cryptsy';

				if(trade.from_goal) {
					if(AppState.game.goal_coin==='USD') {
						AppState.game.remaining_goal += cashin*AppState.game.btc_price;
					} else {
						AppState.game.remaining_goal += cashin;
					}
				}

				_.each(trade.stash, function(coin,id) {
					if(!stash[id]) {
						stash[id] = angular.copy(_.find(_.values(AppState.game.xchg[exchange].mkts),{ primary_currency_code: id }));
					}

					stash[id].qty = (stash[id].qty || 0) + coin.qty;
				});

				$rootScope.success = 'You added ' + _.keys(trade.stash).join(', ') + ' to your stash.';
			}
		};

		return _svc;
	}
])

.controller( 'AppCtrl', [
	'$scope', '$rootScope', '$location', '$timeout', '$fireRef', '$user', '$cookies', 'angularFire', 'AppState', 'GameSvc', '$modal',
	function ( $scope, $rootScope, $location, $timeout, $fireRef, $user, $cookies, angularFire, AppState, GameSvc, $modal ) {
		var loadingChecklist = {
			'securityClearance': false,
			'viewCtrlDone': false
		},
		viewWatch = $scope.$watch('$viewContentLoaded', function(check) {
			viewWatch();

			AppState.game.loading = false;
			AppState.game.loading_complete = true;
		});

		// Instance the tour
		var tour = new Tour();

		// Add your steps. Not too many, you don't really want to get your users sleepy
		tour.addSteps([
			{
				element: "#your_goal",
				title: "Your Trading Goal",
				placement: 'bottom',
				content: "This is what you are aiming for, the object is to cash out your coins at the best price to reduce the Remaining Goal to 0.00 most efficiently."
			},
			{
				element: "#current_stash",
				title: "Your Coin Stash",
				content: "This is where you look at your current coin holdings.  <br /><br />My vision is to allow you to be able to put coins in multiple exchanges and easily switch between them to monitor the relative values."
			},
			{
				element: "#exchange_button",
				placement: 'bottom',
				title: "Exchange Altcoins",
				content: "Click this to exchange coins for each other.  <br /><br />The price is irrelevant since all we're concerned with is the present value of your stash, price only matters when you cash the coins out."
			},
			{
				element: "#coinin_button",
				placement: 'bottom',
				title: "Add Coins",
				content: "Here is where you add coins, you can take cash from your goal or you can just add more coins straightup."
			},
			{
				element: "#cashout_button",
				placement: 'bottom',
				title: "Cash Out",
				content: "This is where you cashout your coins for the current price on the present exchange.  <br /><br />The cash can be applied to the Remaining Goal or used to simply remove coins."
			},
			{
				element: '.tab-heading:first',
				title: 'Market Orders and Graphs',
				placement: 'left',
				content: 'No graphs yet, but you can quickly switch between market tabs of coins in your stash and view the order books which can be sorted by column.'
			},
			{
				element: '#number_format',
				title: 'Custom Number/Decimal Format',
				content: "Last neat thing... Don't like counting 0s?  Switch to Scientific Notation.  <br /><br />Decimal places making your head hurt?  Adjust your 'significant digits' as you see fit. <br /><br />Once I get Saving/Loading Sessions working I'll save these preference with your session.  That's it, let me know what you think, glhf!"
			}
		]);

		// Initialize the tour
		tour.init();

		$scope.state = AppState;

		$rootScope.$watch(function() {
			return $scope.state.game.remaining_goal;
		}, function(next) {
			if(next<=0) {
				setTimeout(function () {
					$scope.openAlert('You have achieved your goal, Congralutations!  You should start over with another goal, EXCELSIOR!','success',15000);	
				},3000);
			}
		});

		// We watch the err so that it can be a channel to the current $alert service setup
		$rootScope.$watch('$alert', function(next,last) {
			if(next) {
				var delay = 4000;

				delete $rootScope.$alert;
				$scope.openAlert(next.msg,next.type,delay);
			}
		});
		$rootScope.$watch('err', function(next,last) {
			if(next) {
				var delay = 7000;

				delete $rootScope.err;
				$scope.openAlert(next,'danger',delay);
			}
		});
		$rootScope.$watch('success', function(next,last) {
			if(next) {
				var delay = 7000;

				delete $rootScope.success;
				$scope.openAlert(next,'success',delay);
			}
		});


		$fireRef.child('xchg/coinbase/mkts/btc_to_usd').on('value', function(snap){
			AppState.game.btc_price =  snap.val();
		});

		$fireRef.child('xchg/cryptsy/error').on('value', function(snap){
			var error = snap.val();

			if(error) {
				$rootScope.err =  error;
				if(!$rootScope.$$phase) {
					$rootScope.$digest();
				}
			}
		});

		$fireRef.child('xchg/cryptsy/last_success').on('value', function(snap){
			var val = snap.val();

			if(val) {
				if(!$scope.state.game.xchg.cryptsy) {
					$scope.state.game.xchg.cryptsy = {};
				}
				$scope.state.game.xchg.cryptsy.last_update = val;
				if(!$rootScope.$$phase) {
					$rootScope.$digest();
				}
			}
		});
		
		$fireRef.child('xchg/coinbase/error').on('value', function(snap){
			var error = snap.val();

			if(error) {
				$rootScope.err = error;
				if(!$rootScope.$$phase) {
					$rootScope.$digest();
				}
			}
		});
		$fireRef.child('xchg/coinbase/last_success').on('value', function(snap){
			var val = snap.val();

			if(val) {
				if(!$scope.state.game.xchg.coinbase) {
					$scope.state.game.xchg.coinbase = {};
				}
				$scope.state.game.xchg.coinbase.last_update = val;
				if(!$rootScope.$$phase) {
					$rootScope.$digest();
				}
			}
		});

		$scope.save = function() {
			var dialog = $modal.open({
					templateUrl: 'app/views/modal-save.html',
					controller: 'SessionCtrl',
					scope: $scope,
					resolve: {
						session: function() {
							var s = _.omit(angular.copy(AppState.game),'xchg');
							_.each(s.player.stash, function(c) {
								delete c.buyorders;
								delete c.sellorders;
							});

							return s;
						}
					}
				});

			dialog.result.then(function(sess) {
				sess.saved_at = Date.now();
				GameSvc.save(sess).then(function(_auth) {
					$rootScope.success = 'Game Created! Redirecting... ';

					$timeout(function() {
						$location.path('/s/'+_auth.d.url);
					},1000);

					if(!$rootScope.$$phase) {
						$rootScope.$digest();
					}
				});
			}, function() {
				// cancelled save action here
			});
		};

		$scope.load = function() {
			var dialog = $modal.open({
					templateUrl: 'app/views/modal-load.html',
					controller: 'SessionCtrl',
					scope: $scope,
					resolve: {
						session: function() {
							return AppState.game;
						}
					}
				});

			dialog.result.then(function(sess) {
				GameSvc.load(sess);
			}, function() {
				// cancelled load action here
			});
		};

		$scope.tour = function () {
			tour.goTo(0);
			// Start the tour
			tour.start(true);
		};

		// Alerts and Error Messages, this should be moved to an $alert service
		$scope.alerts = [];
		// Close alert by id
		$scope.closeAlert = function(id) {
			var idx = _.findIndex($scope.alerts,{ id: id }),
				a;
			if(idx>=0) {
				if(a = $scope.alerts.splice(idx,1)[0]) {
					$timeout.cancel(a.promise);
				}
			}
		};
		// Open alert with Message, Type and Delay
		$scope.openAlert = function(m,t,d) {
			var alert = { type: t, msg: m, id: _.uniqueId() },
				rm = function() {
					$scope.closeAlert(alert.id);
				};

			if(d) {
				alert.promise = $timeout(rm, d);
			}
			
			$scope.alerts.push(alert);
		};
		// END $alert service
	}
])

.controller('SessionCtrl', [ 
	'$scope', 'session', '$modalInstance',
	function ( $scope, session, $modalInstance ) {
		console.log('initializing save controller');
		$scope.session = session;

		$scope.ok = $modalInstance.close;
	}
])

.controller('DefaultCtrl', [ '$scope',
	function ( $scope ) {
	}
])

.controller('StartCtrl', [ 
	'$scope', '$rootScope', '$location', '$fireRef', 'GameSvc', '$modal',
	function ( $scope, $rootScope, $location, $fireRef, GameSvc, $modal ) {
		var gamesRef = $fireRef.child('games');

		// GameSvc.xlisten();

		$scope.xchg = {
			coins: [
				// { name: 'Bitcoin', abbr: 'BTC', id: '1' },
				// { name: 'Litecoin', abbr: 'LTC', id: '2' },
				// { name: 'Primecoin', abbr: 'XPM', id: '3' },
				// { name: 'Stablecoin', abbr: 'SBC', id: '4' }
			]
		};

		$scope.$watch(function() {
			return $scope.state.game.active_exchange;
		}, function(next) {
			if(next) {
				$fireRef.child('xchg').child(next).child('mkts').on('value', function(snap) {
					var val = snap.val(),
						exchange = next,
						stash = $scope.state.game.player.stash;

					if(!$scope.state.game.xchg[exchange]) {
						$scope.state.game.xchg[exchange] = {};
					}
					$scope.state.game.xchg[exchange].mkts = val;

					_(val).values().filter(function(mkt) {
						return mkt.secondary_currency_code==='BTC' && _.contains(_.keys(stash), mkt.primary_currency_code);
					}).each(function(mkt) {
						angular.extend(stash[mkt.primary_currency_code], _.omit(mkt,['buyorders','sellorders','recenttrades']));
					});

					$scope.xchg.coins = _.filter(_.values(val),function(mkt) {
						return mkt.secondary_currency_code==='BTC';
					});

					if(!$scope.$$phase) {
						$scope.$digest();
					}
				});
			}
		});

		$scope.addCoins = function() {
			$('#add_coin_qty').trigger('focus');
		};

		$scope.focus = function(target) {
			$(target).trigger('focus');
		};

		$scope.addToStash = function(coin) {
			GameSvc.addToStash(coin);
			$scope.addCoins();
		};

		$scope.setGoal = function() {
			$scope.state.game.set_freecoin = true;
			GameSvc.setGoal.apply(GameSvc,arguments);
			setTimeout(function() {
				$('#add_coin_qty').trigger('focus');
			},400);
		};
		$scope.start = GameSvc.start;
		$scope.loadOrders = GameSvc.loadOrders;
		$scope.coinIn = function() {
			var dialog = $modal.open({
					templateUrl: 'app/views/modal-coinin.html',
					controller: 'ModalCtrl',
					scope: $scope,
					resolve: {
						stash: function() {
							return {};
						}
					}
				});

			dialog.result.then(function(x) {
				GameSvc.coinIn(x);
			}, function() {
				// cancel coin in action here
			});
		};
		$scope.cashOut = function() {
			var dialog = $modal.open({
					templateUrl: 'app/views/modal-coinout.html',
					controller: 'ModalCtrl',
					scope: $scope,
					resolve: {
						stash: function() {
							return angular.copy($scope.state.game.player.stash);
						}
					}
				});

			dialog.result.then(function(x) {
				GameSvc.cashOut(x);
			}, function() {
				// cancel cash out action here
			});
		};
		$scope.exchange = function() {
			var dialog = $modal.open({
					templateUrl: 'app/views/modal-exchange.html',
					controller: 'ModalCtrl',
					scope: $scope,
					resolve: {
						stash: function() {
							return angular.copy($scope.state.game.player.stash);
						}
					}
				});

			dialog.result.then(function(x) {
				GameSvc.exchange(x);
			}, function() {
				// cancel exchange action here
			});
		};

		$scope.pump = function() {
			alert('coming soon, maybe... check Contributions for more information');
		};

		$scope.ticker = function() {
			alert('coming soon, maybe... check Contributions for more information');
		};
	}
])

.controller('ModalCtrl', [ '$scope', '$modalInstance', 'stash', 'GameSvc',
	function ( $scope, $modalInstance, stash, GameSvc ) {
		$scope.stash = stash;
		$scope.trade = { stash: stash, from_goal: true, toward_goal: true };

		$scope.addToTrade = function(coin) {
			GameSvc.addToStash(coin,$scope.trade.stash);
		};

		$scope.ok = $modalInstance.close;

		$scope.cancel = function () {
			$modalInstance.dismiss('cancel');
			delete $scope.trade;
		};
	}
])

.controller('ContributeCtrl', [ 
	'$scope', '$rootScope', '$http',
	function($scope,$rootScope,$http) {
		$scope.submit = function(post) {
			$http({
				url: '/feedback',
				data: post,
				method: 'POST'
			}).then(function(res) {
				$rootScope.success = res.data;
				_.forOwn(post,function(v,k) { delete post[k]; });

				console.log('what the fuck is up?', post);
				if(!$scope.$$phase) {
					$scope.$digest();
				}
			}, function(res) {
				$rootScope.err = res.data;
			});
		};
	}
])

;
