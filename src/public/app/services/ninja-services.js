angular.module( 'njaServices', [ 'firebase' ])
.provider('$fireRef', function() {
	this.options = {};

	this.$get = function() {
		return new Firebase(this.options.url);
	};
})

.service('$invite', [
	'$http', '$q',
	function($http, $q) {
		return {
			send: function(email) {
				var dfr = $q.defer();

				$http({
					method: 'POST',
					url: '/invite',
					data: { email: email }
				}).then(function(res) {
					var data = res.data;
					
					dfr.resolve(data);
				}, function(res) {
					dfr.reject(res.data);
				});

				return dfr.promise;
			}
		};
	}
])

.service('$user', [
	'$http', '$fireRef', '$rootScope', '$q', 'angularFire', 'angularFireAuth', 'AppState',
	function($http, $fireRef, $rootScope, $q, angularFire, angularFireAuth, AppState) {
		var _dfr = $q.defer(),
			_login = $q.defer(),
			_logout;


		// Handle Login Event
		$rootScope.$on("angularFireAuth:login", function(e, auth) {
			// User logged in
			$fireRef.child('games').child(auth.d.url).once('value', function(snap) {
				delete AppState.game.loading;
				AppState.game.auth = snap.val();
			});
		});

		// Handle Logout Event
		$rootScope.$on("angularFireAuth:logout", function(e) {
			// User logged out.
			console.log('user is logged out now');
			delete AppState.game.auth;
		});

		// Handle Auth Error Event
		$rootScope.$on("angularFireAuth:error", function(e, err) {
			// There was an error during authentication.
			console.log('angular auth error', err);

			// delete $rootScope.user;
			delete AppState.game;
			$rootScope.err = err;
		});

		$http({
			method: 'GET',
			url: '/user/login'
		}).then(function(res) {
			var data = res.data;

			console.log('response from login', data);

			angularFireAuth.initialize($fireRef,{
				scope: $rootScope,
				name: 'auth',
				path: '/login',
				callback: function(err,auth) {
					if(err) {
						return _dfr.reject(err);
					}

					_dfr.resolve(auth);
				}
			});

			if(data.err) {
				_dfr.reject(data.err);
			} else if(data.token) {
				_dfr.resolve(data.token);
				console.log(data.token);
				angularFireAuth.login(data.token);
			} else {
				console.log(res);
				_dfr.reject('Unkown Error');
			}

		}, function(res) {
			$rootScope.err = res.data;
		});


		var svc = {
			// init: _dfr.promise,
			logged_in: _login.promise,
			auth: function(dfr) {
				return function(res) {
					console.log('custom login response',res);
					var data = res.data;

					if(data.token) {
						angularFireAuth.login(data.token).then(function(_user) {
							dfr.resolve(angular.extend(_user,data));
						}, function(err) {
							dfr.reject(err);
						});
						
					} else if(data.err) {
						dfr.reject(data.err);
					} else {
						console.log('??? ',data);
						dfr.reject('Unkown authentication error');
					}

					return dfr.promise;
				};
			},
			verify: function(uid) {
				// POST to /user/verify with id to verify email address for the user in session
				var dfr = $q.defer();

				console.log('verify user id', uid);

				$http({
					method: 'POST',
					url: '/user/verify',
					data: { uid: uid }
				}).then(function(res) {
					var data = res.data;

					if(data.err) {
						$rootScope.err = data.err;
						return dfr.reject(data.err);
					}
					
					dfr.resolve(res.data);
				}, function(res) {
					dfr.reject(res.data);
				});

				return dfr.promise;
			},
			lookup: function(by,q) {
				var dfr = $q.defer();

				console.log('user lookup by', by, q);

				$http({
					method: 'GET',
					url: '/user/lookup/'+by,
					params: { query: q }
				}).then(function(res) {
					var data = res.data;
					
					console.log('looked up', data);
					
					if(data.err) {
						dfr.reject(data.err);
					} else {
						dfr.resolve(data);
					}
				}, function(res) {
					console.log('lookup err', res.data);
					dfr.reject(res.data);
				});

				return dfr.promise;
			},
			create: function(session) {
				var dfr = $q.defer();

				$http({
					method: 'POST',
					url: '/user/create',
					data: session
				}).then(svc.auth(dfr), dfr.reject);

				return dfr.promise;
			},
			update: function(_user) {
				var dfr = $q.defer();

				$http({
					method: 'POST',
					url: '/user/update',
					data: _user
				}).then(function(res) {
					var data = res.data;

					if(data.err) {
						dfr.reject(data.err);
					} else if(data.success) {
						dfr.resolve(data.success);
					} else {
						console.log('Unknown update error',data);
						dfr.reject('Unknown update error');
					}
				}, dfr.reject);

				return dfr.promise;
			},
			login: function(_auth) {
				var dfr = $q.defer();

				$http({
					method: 'POST',
					url: '/user/login',
					data: _auth
				}).then(svc.auth(dfr), dfr.reject);

				return dfr.promise;
			},
			join: function(uid) {
				var dfr = $q.defer();

				$http({
					method: 'POST',
					url: '/user/join',
					data: { uid: uid }
				}).then(function(res) {
					dfr.resolve(res.data);
				}, function(res) {
					dfr.reject(res.data);
				});

				return dfr.promise;
			},
			'forgotPw': function(email){
				return $http({
					method: 'POST',
					url: '/user/forgot-password',
					data: { email: email }
				});
			},
			'resetPw': function(data) {
				return $http({
					method: 'POST',
					url: '/user/reset',
					data: data
				});
			},
			logout: function() {
				return $http({
					method: 'POST',
					url: '/user/logout'
				}).then(function(res) {
					console.log('angular logout!!!', res);
					if(res.data.success) {
						angularFireAuth.logout();
					}
					return res;
				});
			}
		};

		return svc;
	}
])

;