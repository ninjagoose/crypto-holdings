// Boilerplate Express Web Server
"use strict";

// Load main web application server modules
var express = require('express'),
	app = express(),
	port = process.env.PORT || 5000,
	
	// Load utility modules
	http = require('http'),
	path = require('path'),
	_ = require('lodash-node'),
	Q = require('q'),

	bcrypt = require('bcrypt'),
	shortId = require('shortid'),
	EmailTemplateLoader = require('email-templates'),

	// Application Keys and Secrets, should be kept elsewhere
	SESSION_SECRET = '$2a$12$bF3KEdJTxITj/us1DkH20.u.V9yVWry8RC1b9qNQeRoPXLDZitxfu',
	POSTMARK_API_KEY = process.env['POSTMARK_API_KEY'] || 'c9659a82-9e44-4ac1-b0ca-2542f2e420d4',
	FIREBASE_SECRET = process.env['FIREBASE_SECRET'] || 'UK7y40Gdv7CcvsAMBwdpnyZ09qNDFWEhZX4nOtQd',
	
	// Connection URLs
	MONGO_URL = process.env['MONGOHQ_URL'] ||  'mongodb://localhost/piqpic',
	FIREBASE_URL =  process.env['FIREBASE_URL'] || 'https://piqpic.firebaseio.com/',

	// MongoDB storage
	MongoClient = require('mongodb').MongoClient,
	MongoObjectId = require('mongodb').ObjectID,
	MongoStore = require('connect-mongo')(express),

	// Firebase storages
	Firebase = require('firebase'),
	FirebaseTokenGenerator = require("firebase-token-generator"),
	fireRef = new Firebase(FIREBASE_URL),
	tokenGenerator = new FirebaseTokenGenerator(FIREBASE_SECRET),

	// Paths
	controllerPath = path.resolve(__dirname, 'build', 'src', 'server', 'controllers'),	
	emailTemplatePath = path.resolve(__dirname, 'build', 'src', 'app', 'templates', 'emails'),
	
	// Email Service
	Postmark = require("postmark")(POSTMARK_API_KEY),

	// Initialization promises
	dfr_EmailTemplateLoading = Q.defer(),
	dfr_DatabaseConnecting = Q.defer(),
	dfr_ServerListening = Q.defer(),

	// Error loggin
	logError = function(err) {
		console.log(err);
	};

// Express Configuration and Middleware Stack
app.configure(function(){
	// Sessions and Cookies
	app.use(express.cookieParser());
	app.use(express.session({
		store: new MongoStore({
			url: MONGO_URL,
			auto_reconnect: true
		}),
		secret: SESSION_SECRET
	}));

	// Static serving
	app.use(express.static(__dirname + '/build/'));
	app.use(express.favicon(__dirname + '/build/assets/favicon.ico'));

	// Parse request body
	app.use(express.bodyParser());

	// App server router
	app.use(app.router);

	// 404 serving
	app.use(function(req,res,next) {
		res.status(404).send('404 - Not Found');
	});

	// Error serving
	app.use(function(err, req, res, next){
		console.error(err.stack);
		res.status(500).send('500 - Unknown Server Error, terribly sorry');
	});
});

// After server begins listening
dfr_ServerListening.promise.then(function() {
	console.log('Listening on port ' + port);
}, logError);

// After email templates are loaded and mongo database connection established
Q.all([ dfr_DatabaseConnecting.promise, dfr_EmailTemplateLoading.promise ]).spread(function(DB, EmailTemplates) {
	// DB collection models
	var Users = DB.collection('users');

	// Routing Controllers
	// - Create new user
	app.post('/user/create/?', require(controllerPath + '/create-user')(Users,fireRef,Postmark,EmailTemplates,tokenGenerator));
	// - Verify user
	app.post('/user/verify', require(controllerPath + '/verify-user')(Users,fireRef));
	// - Get User login token
	app.get('/user/login/?', require(controllerPath + '/login-token')());
	// - Log user in
	app.post('/user/login/?', require(controllerPath + '/login-user')(Users,tokenGenerator));
	// - Log user out
	app.post('/user/logout', require(controllerPath + '/logout-user')());
	// - User forgot password
	app.post('/user/forgot-password/?', require(controllerPath + '/forgot-password')(Users,Postmark,EmailTemplates));
	// - Lookup user by reset id
	app.get('/user/lookup/reset/?', require(controllerPath + '/lookup-reset')(Users,fireRef,MongoObjectId));
	// - User Join from invitation
	app.post('/user/join/?', require(controllerPath + '/user-join')(Users,fireRef,MongoObjectId));
	// - Invite new users
	app.post('/invite/?', require(controllerPath + '/invite-user')(Users,fireRef,MongoObjectId,EmailTemplates,Postmark));
	// - User profile
	app.post('/user/update', require(controllerPath + '/update-user')(Users,fireRef,MongoObjectId));

	// - Catch all the errors that aren't explicitly thrown
	app.all('*', require(controllerPath + '/check-error')());

	// Create server
	http.createServer(app).listen(port, dfr_ServerListening.makeNodeResolver());
}, logError);

// Load email templates
EmailTemplateLoader(emailTemplatePath, dfr_EmailTemplateLoading.makeNodeResolver());
// Connect to mongo database
MongoClient.connect(MONGO_URL, dfr_DatabaseConnecting.makeNodeResolver());
